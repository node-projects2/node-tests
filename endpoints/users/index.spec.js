const handlers = require('./index');
// primer parámetro indica lo que vamos a testear.
describe('EndPoints', () => {
    describe('users', () => {
        describe('get', () => {
            it('devuelve json con los usuarios', async () => {
                const axiosMock = {
                    get: jest.fn().mockResolvedValue({ data: 1 })
                };
                console.log(axiosMock.get);
                // mockReturnThis para que devuelva el mismo objeto para hacer luego el send
                const responseMock = {
                    status: jest.fn().mockReturnThis(),
                    send: jest.fn()
                };

                await handlers({ axios: axiosMock }).get({}, responseMock);
                // console.log(responseMock.status.mock.calls);

                expect(responseMock.status.mock.calls).toEqual([[200]]);
                expect(responseMock.send.mock.calls).toEqual([[1]]);

            });
        });
        describe('post', () => {
            it('Crea un usuario', async () => {
                const axiosMock = {
                    post: jest.fn().mockResolvedValue({ data: 1 })
                };
                const responseMock = {
                    status: jest.fn().mockReturnThis(),
                    send: jest.fn()
                };
                const req = {
                    body: 'request body'
                };

                await handlers({ axios: axiosMock }).post(req, responseMock);

                expect(responseMock.status.mock.calls).toEqual([[201]]);
                expect(responseMock.send.mock.calls).toEqual([[1]]);
                // parámetros de llamada para el post
                expect(axiosMock.post.mock.calls).toEqual([
                    ['https://jsonplaceholder.typicode.com/users', 'request body']
                ]);

            });
        });
        describe('put', () => {
            it('Modifica un usuario', async () => {
                const axiosMock = {
                    put: jest.fn().mockResolvedValue({ data: 1 })
                };
                const responseMock = {
                    sendStatus: jest.fn().mockReturnThis()
                };
                const req = {
                    body: 'request body',
                    params: {
                        id: 12
                    }
                };

                await handlers({ axios: axiosMock }).put(req, responseMock);

                expect(responseMock.sendStatus.mock.calls).toEqual([
                    [204]
                ]);
                // parámetros de llamada para el post
                expect(axiosMock.put.mock.calls).toEqual([
                    ['https://jsonplaceholder.typicode.com/users/12', 'request body']
                ]);

            });
        });

        describe('delete', () => {
            it('Borra un usuario', async () => {
                const axiosMock = {
                    delete: jest.fn()
                };
                // como no devuelve nada, se le puede poner sólo fn()
                const responseMock = {
                    sendStatus: jest.fn()
                };
                const req = {
                    body: 'request body',
                    params: {
                        id: 54
                    }
                };

                await handlers({ axios: axiosMock }).delete(req, responseMock);

                expect(responseMock.sendStatus.mock.calls).toEqual([
                    [204]
                ]);
                // parámetros de llamada para el post
                expect(axiosMock.delete.mock.calls).toEqual([
                    ['https://jsonplaceholder.typicode.com/users/54']
                ]);
                expect(responseMock.sendStatus.mock.calls).toEqual([
                    [204]
                ]);
            });
        });
    });
});