var express = require('express');
const axios = require('axios');
const parser = require('body-parser');
const { users } = require('./endpoints');
var app = express();

// parse application/x-www-form-urlencoded
app.use(parser.urlencoded({ extended: false }));

// parse application/json
app.use(parser.json());

const usersHandlers = users({ axios });

app.get('/', usersHandlers.get);
app.post('/', usersHandlers.post);
app.put('/:id', usersHandlers.put);
app.delete('/:id', usersHandlers.delete);

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});